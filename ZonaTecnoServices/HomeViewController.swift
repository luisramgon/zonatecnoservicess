//
//  HomeViewController.swift
//  ZonaTecnoServices
//
//  Created by Jose Luis Ramirez Gonzalez on 22/02/22.

import UIKit
import FirebaseAuth

enum ProviderType: String {
    case basic
    case google

class HomeViewController: UIViewController {
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var providerLabel: UILabel!
    @IBOutlet weak var closeSessionButton: UIButton!
    
    private let email: String
    private let provider: ProviderType
    
    init(email: String, provider: ProviderType) {
        self.email = email
        self.provider = provider
        super.init(nibName:"HomeViewController", bundle:nil)
    }
    
    required init (coder: NSCoder){
        fatalError("init(coder) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Inicio"
        navigationItem.setHidesBackButton(true, animated: false)
        
        emailLabel.text = email
        providerLabel.text = provider.rawValue
        
            
        // Guarda los datos del usuario
        let defaults = UserDefaults.standard
        defaults.set(email, forKey: "email")
        defaults.set(provider.rawValue, forKey: "provider")
        defaults.synchronize()
    
    }
    
    @IBAction func ClseSessionButtonAction(_ sender: Any) {
        
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "email")
        defaults.removeObject(forKey: "provider")
        defaults.synchronize()
        
        switch provider {
        
        case .basic, .google:
            do {
                try Auth.auth().signOut()
                navigationController?.popViewController(animated: true)
            } catch {
            // Se ha producido un error
            }
        }
    }
    
    
}
    
}
